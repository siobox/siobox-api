// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SioBox.h"

/**
 * 
 */
class SIOBOX_API SioboxPaths
{
public:
	SioboxPaths();
	~SioboxPaths();

	static void GetGameDirectory(char* OutGameDirectory, int& InCapacity);
	static void GetLogDirectory(char* InOutLogDirectory, int& InCapacity);
	static void GetAddonsDirectory(char* InOutAddonsDirectory, int& InCapacity);
	static void GetPluginsDirectory(char* InOutPluginsDirectory, int& InCapacity);
	static void GetSavesDirectory(char* InOutSavesDirectory, int& InCapacity);
	static void GetLaunchDirectory(char* InOutLaunchDirectory, int& InCapacity);
	static void GetCombine(const char* InPathA, const char* InPathB, char* InOutCombinedPath, int& InCapacity);

};
