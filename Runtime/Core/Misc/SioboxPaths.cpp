// Fill out your copyright notice in the Description page of Project Settings.

#include "SioboxPaths.h"
#include "Paths.h"

SioboxPaths::SioboxPaths()
{

}

SioboxPaths::~SioboxPaths()
{
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxPaths::, Paths_, GetGameDirectory, (char* a, int& b), (a, b))
void SioboxPaths::GetGameDirectory(char* OutGameDir, int& InCapacity) {
	strncpy(OutGameDir, TCHAR_TO_ANSI(*FPaths::ProjectDir()), InCapacity);
}
DECLARE_SIOBOX_STATIC_FUNCTION(SioboxPaths::, Paths_, GetLogDirectory, (char* a, int& b), (a, b))
void SioboxPaths::GetLogDirectory(char* OutLogDirectory, int& InCapacity) {
	strncpy(OutLogDirectory, TCHAR_TO_ANSI(*FPaths::ProjectLogDir()), InCapacity);
}
DECLARE_SIOBOX_STATIC_FUNCTION(SioboxPaths::, Paths_, GetAddonsDirectory, (char* a, int& b), (a, b))
void SioboxPaths::GetAddonsDirectory(char* OutAddonsDirectory, int& InCapacity) {
	strncpy(OutAddonsDirectory, TCHAR_TO_ANSI(*FPaths::Combine(FPaths::ProjectDir(), TEXT("Addons"))), InCapacity);
}
DECLARE_SIOBOX_STATIC_FUNCTION(SioboxPaths::, Paths_, GetPluginsDirectory, (char* a, int& b), (a, b))
void SioboxPaths::GetPluginsDirectory(char* OutPluginsDirectory, int& InCapacity) {
	strncpy(OutPluginsDirectory, TCHAR_TO_ANSI(*FPaths::ProjectPluginsDir()), InCapacity);

}
DECLARE_SIOBOX_STATIC_FUNCTION(SioboxPaths::, Paths_, GetSavesDirectory, (char* a, int& b), (a, b))
void SioboxPaths::GetSavesDirectory(char* OutSavesDirectory, int& InCapacity) {
	strncpy(OutSavesDirectory, TCHAR_TO_ANSI(*FPaths::ProjectSavedDir()), InCapacity);

}
DECLARE_SIOBOX_STATIC_FUNCTION(SioboxPaths::, Paths_, GetLaunchDirectory, (char* a, int& b), (a, b))
void SioboxPaths::GetLaunchDirectory(char* OutLaunchDirectory, int& InCapacity) {
	strncpy(OutLaunchDirectory, TCHAR_TO_ANSI(*FPaths::LaunchDir()), InCapacity);

}
DECLARE_SIOBOX_STATIC_FUNCTION(SioboxPaths::, Paths_, GetCombine, (const char* a, const char* b, char* c, int& d), (a, b, c, d))
void SioboxPaths::GetCombine(const char* InPathA, const char* InPathB, char* OutCombinedPath, int& InCapacity) {
	strncpy(OutCombinedPath, TCHAR_TO_ANSI(*FPaths::Combine(ANSI_TO_TCHAR(InPathA), ANSI_TO_TCHAR(InPathB))), InCapacity);
}
