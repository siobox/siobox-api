// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Siobox.h"
#include "Engine.h"

//~~~~~ Forward Declarations~~~~~

class USioboxInstance;

class SIOBOX_API SioboxTimerManager
{

	static TSharedPtr<USioboxInstance> m_instance;

public:
	SioboxTimerManager(USioboxInstance* instance);
	~SioboxTimerManager();

	static void StartTimer(FTimerHandle &InOutHandle, float InRate, bool InbLoop, float InFirstDelay, void* InCallback);
	static void StopTimer(FTimerHandle* InHandle);
	static void IsTimerPending(FTimerHandle* InHandle, bool &OutbPending);
	static void IsTimerExists(FTimerHandle* InHandle, bool &OutbExists);
	static void IsTimerPaused(FTimerHandle* InHandle, bool &OutbPaused);
	static void IsTimerActive(FTimerHandle* InHandle, bool &OutbActive);
	static void HasBeenTickedThisFrame(bool &OutbTicked);
	static void UnpauseTimer(FTimerHandle* InHandle);
	static void PauseTimer(FTimerHandle* InHandle);
	static void GetTimerElapsed(FTimerHandle* InHandle, float &OutElapsedTime);
	static void GetTimerRate(FTimerHandle* InHandle, float &OutRate);
	static void GetTimerRemaining(FTimerHandle* InHandle, float &OutRemainingTime);
};