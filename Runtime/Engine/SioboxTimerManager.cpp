// Fill out your copyright notice in the Description page of Project Settings.

#include "SioboxTimerManager.h"
#include "SioboxInstance.h"

TSharedPtr<USioboxInstance> SioboxTimerManager::m_instance = NULL;

SioboxTimerManager::SioboxTimerManager(USioboxInstance* instance) {
	if(!m_instance.IsValid()) m_instance = MakeShareable(instance);
}

SioboxTimerManager::~SioboxTimerManager() {

}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, StartTimer, (FTimerHandle &a, const float b, const bool c, const float d, void* e), (a, b, c, d, e));
void SioboxTimerManager::StartTimer(FTimerHandle &OutHandle, const float InRate, const bool InbLoop, const float InFirstDelay, void* InCallback) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
		return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	FTimerHandle handle;
	FTimerDelegate fDelegate;
	fDelegate.BindLambda([=]() {
		static_cast<void(*)()>(InCallback)();
	});

	world->GetTimerManager().SetTimer(handle, InRate, InbLoop, InFirstDelay);
	OutHandle = handle;
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, StopTimer, (FTimerHandle *a), (a));
void SioboxTimerManager::StopTimer(FTimerHandle* InHandle) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	world->GetTimerManager().ClearTimer(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, IsTimerPending, (FTimerHandle *a, bool &b), (a, b));
void SioboxTimerManager::IsTimerPending(FTimerHandle* InHandle, bool &OutbPending) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	OutbPending = world->GetTimerManager().IsTimerPending(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, IsTimerExists, (FTimerHandle *a, bool &b), (a, b));
void SioboxTimerManager::IsTimerExists(FTimerHandle* InHandle, bool &OutbExists) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	OutbExists = world->GetTimerManager().TimerExists(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, IsTimerPaused, (FTimerHandle *a, bool &b), (a, b));
void SioboxTimerManager::IsTimerPaused(FTimerHandle* InHandle, bool &OutbPaused) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	OutbPaused = world->GetTimerManager().IsTimerPaused(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, IsTimerActive, (FTimerHandle *a, bool &b), (a, b));
void SioboxTimerManager::IsTimerActive(FTimerHandle* InHandle, bool &OutbActive) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	OutbActive = world->GetTimerManager().IsTimerActive(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, HasBeenTickedThisFrame, (bool &a), (a));
void SioboxTimerManager::HasBeenTickedThisFrame(bool &OutbTicked) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	OutbTicked = world->GetTimerManager().HasBeenTickedThisFrame();
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, UnpauseTimer, (FTimerHandle* a), (a));
void SioboxTimerManager::UnpauseTimer(FTimerHandle* InHandle) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	world->GetTimerManager().UnPauseTimer(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, PauseTimer, (FTimerHandle* a), (a));
void SioboxTimerManager::PauseTimer(FTimerHandle* InHandle) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	world->GetTimerManager().PauseTimer(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, GetTimerElapsed, (FTimerHandle* a, float &b), (a, b));
void SioboxTimerManager::GetTimerElapsed(FTimerHandle* InHandle, float &OutElapsedTime) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	OutElapsedTime = world->GetTimerManager().GetTimerElapsed(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, GetTimerRate, (FTimerHandle* a, float &b), (a, b));
void SioboxTimerManager::GetTimerRate(FTimerHandle* InHandle, float &OutRate) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	OutRate = world->GetTimerManager().GetTimerRate(*InHandle);
}

DECLARE_SIOBOX_STATIC_FUNCTION(SioboxTimerManager::, TimerManager_, GetTimerRemaining, (FTimerHandle* a, float &b), (a, b));
void SioboxTimerManager::GetTimerRemaining(FTimerHandle* InHandle, float &OutRemainingTime) {
	if (!m_instance.IsValid()) {
		UE_LOG(SioboxAPI, Fatal, TEXT("GameInstance is null"))
			return;
	}

	USioboxInstance* instance = m_instance.Get();
	UWorld* world = instance->GetWorld();
	OutRemainingTime = world->GetTimerManager().GetTimerRemaining(*InHandle);
}
