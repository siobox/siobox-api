﻿# Siobox Unmanaged C++ API

### Overview
All api methods are __static__ _voids_ and results to return are in parameters [ptr or ref].
Above api method in __CPP__ must be __DECLARE_SIOBOX_STATIC_FUNCTION__ macro.

### Definitions
`DECLARE_SIOBOX_STATIC_FUNCTION`
This will generate correct EXPORT NAME for to be used in CSharp API and it is __HACK__ for 'extern "C"' in C++ classes.


```cpp
(NAMESPACE, PREFIX, METHOD_NAME, (METHOD_ARGUMENTS), (METHOD_ARGUMENTS_TO_CALL_METHOD_ARGUMENTS))
```
That will be converted to
```cpp
extern "C" void SIOBOX_FUNCTION SIOBOX_PREFIXMETHOD_NAME METHOD_ARGUMENTS { NAMESPACEMETHOD_NAME METHOD_ARGUMENTS_TO_CALL_METHOD_ARGUMENTS; }
```

More Clearly
```cpp
extern "C" void SIOBOX_FUNCTION /* 'SIOBOX_' + PREFIX + METHOD_NAME */ METHOD_ARGUMENTS {
    /* NAMESPACE + METHOD_NAME  METHOD_ARGUMENTS_TO_CALL_METHOD_ARGUMENTS */
}
```
__Do not forget__ to add parentheses at __METHOD_ARGUMENTS__ and __METHOD_ARGUMENTS_TO_CALL_METHOD_ARGUMENTS__


### Example
#### Incorrect Usage
```cpp
bool DoThis() {
    return true;
}
```
#### Correct Usage
```cpp
DECLARE_SIOBOX_STATIC_FUNCTION(MyClass::, MyClass_, DoThis, (bool& a), (a))
void MyClass::DoThis(bool& InbTest) {
    InbTest = true;
}
```
```EXPORT= 'SIOBOX_' + PREFIX + MethodName ```
```EXPORT= SIOBOX_MyClass_DoThis```


