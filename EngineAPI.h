// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "SioBox.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EngineAPI.generated.h"

/**
 * 
 */
UCLASS()
class SIOBOX_API UEngineAPI : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UEngineAPI() {}

	static void* DllHandle;

	UFUNCTION(BlueprintCallable, Category = "SioBoxAPI")
	static void testFunction();

	UFUNCTION(BlueprintCallable, Category = "SioBoxAPI")
	static bool loadAPI();

	UFUNCTION(BlueprintCallable, Category = "SioBoxAPI")
	static bool unloadAPI();

	UFUNCTION(BlueprintCallable, Category = "SioBoxAPI")
	static bool loaded();

	static void* getFunction(FString functionName);

	static void EngineLog(const char* log);
	static void EngineError(const char* error);

	UFUNCTION(BlueprintCallable, Category = "SioBoxAPI")
	static void initializeEngine();

	UFUNCTION(BlueprintCallable, Category = "SioBoxAPI")
	static void deinitializeEngine();

	UFUNCTION(BlueprintCallable, Category = "SioBoxAPI")
	static void addFakeAddon(FString addonName);

	UFUNCTION(BlueprintCallable, Category = "SioBoxAPI")
	static void listFakeAddons();
};