// Fill out your copyright notice in the Description page of Project Settings.

#include "EngineAPI.h"

void* UEngineAPI::DllHandle = nullptr;

DECLARE_SIOBOX_STATIC_FUNCTION(UEngineAPI::, Engine_, EngineLog, (const char* a), (a));
void UEngineAPI::EngineLog(const char* log)  {
	FString message = ANSI_TO_TCHAR(log);
	UE_LOG(SioboxAPI, Display, TEXT("[API] Log: %s"), *message);
}

DECLARE_SIOBOX_STATIC_FUNCTION(UEngineAPI::, Engine_, EngineError, (const char* a), (a));
void UEngineAPI::EngineError(const char* error) {
	UE_LOG(SioboxAPI, Display, TEXT("SioBox_Engine_EngineError"));

	FString message = ANSI_TO_TCHAR(error);
	UE_LOG(SioboxAPI, Error, TEXT("ENGINE ERROR: %s"), *message);
}

bool UEngineAPI::loadAPI() {
	if (UEngineAPI::DllHandle != nullptr) {
		UE_LOG(SioboxAPI, Error, TEXT("Trying to load API when is already loaded!"))
		return false;
	}
	else {
		FString filePath = FPaths::Combine(*FPaths::ProjectPluginsDir(), FPaths::Combine(TEXT("x64"), TEXT("SioBoxAPI.dll")));
		if (FPaths::FileExists(filePath)) {
			UEngineAPI::DllHandle = FPlatformProcess::GetDllHandle(*filePath);
		}
		else {
			UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI file not found. [%s]"), *filePath);
		}
	}
	bool loaded = UEngineAPI::loaded();
	if (!loaded) UE_LOG(SioboxAPI, Fatal, TEXT("SioBoxAPI not loaded."))
	else UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI successfully loaded."));
	return loaded;
}

bool UEngineAPI::loaded() {
	return UEngineAPI::DllHandle != nullptr;
}

void* UEngineAPI::getFunction(FString functionName) {
	if (!UEngineAPI::loaded()) { 
		UE_LOG(SioboxAPI, Error, TEXT("Cannot get function when SioBoxAPI is not loaded."))
		return nullptr; 
	}
	UE_LOG(SioboxAPI, Verbose, TEXT("Getting function [%s] was successful."), *functionName);
	return FPlatformProcess::GetDllExport(UEngineAPI::DllHandle, *functionName);
}

typedef void(*_SioBoxAPI_INITIALIZE_ENGINE)(bool &OutbSuccess);
static _SioBoxAPI_INITIALIZE_ENGINE m_initializeEngineFromDll;

void UEngineAPI::initializeEngine() {
	UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI Initializating engine..."));
	m_initializeEngineFromDll = _SioBoxAPI_INITIALIZE_ENGINE(getFunction("SioBoxAPI_INITIALIZE_ENGINE"));
	if (m_initializeEngineFromDll != nullptr) {
		bool success = false;
		UE_LOG(SioboxAPI, Display, TEXT("Initializing engine NOW"));
		m_initializeEngineFromDll(success);

		if(success) {
			UE_LOG(SioboxAPI, Display, TEXT("Initializating engine done"));
			return;
		}
	}
	
	UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI Initializating engine failed"));
}


typedef void(*_SioBoxAPI_DEINITIALIZE_ENGINE)();
static _SioBoxAPI_DEINITIALIZE_ENGINE m_deinitializeEngineFromDll;

void UEngineAPI::deinitializeEngine() {
	UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI Deinitializating engine..."));
	m_deinitializeEngineFromDll = _SioBoxAPI_DEINITIALIZE_ENGINE(UEngineAPI::getFunction("SioBoxAPI_DEINITIALIZE_ENGINE"));
	if (m_deinitializeEngineFromDll != nullptr) {
		try {
			m_deinitializeEngineFromDll();
			UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI deinitialized"));
		}
		catch (int e) {
			UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI_DEINITIALIZE_ENGINE EXCEPTION %s"), *FString::FromInt(e))
		}
		return;
	}
	UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI Deinitializing engine failed"));
}

typedef void(*_SioBoxAPI_Engine_TestFunction)();
static _SioBoxAPI_Engine_TestFunction m_testFunctionFromDll;

void UEngineAPI::testFunction()
{
	UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI_Engine_TestFunction"));
	m_testFunctionFromDll = _SioBoxAPI_Engine_TestFunction(UEngineAPI::getFunction("SioBoxAPI_Engine_TestFunction"));
	if (m_testFunctionFromDll != nullptr) {
		try {
			m_testFunctionFromDll();
			UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI_Engine_TestFunction executed successfully!"));
		}
		catch (int e) {
			UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI_Engine_TestFunction EXCEPTION %s"), *FString::FromInt(e))
		}
		return;
	}
	UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI_Engine_TestFunction failed"));
}


typedef void(*_SioBoxAPI_Engine_AddFakeAddon)(FString addonName);
static _SioBoxAPI_Engine_AddFakeAddon m_addFakeAddonFromDll;


void UEngineAPI::addFakeAddon(FString addonName)
{
	UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI_Engine_AddFakeAddon(%s)"), *addonName);
	m_addFakeAddonFromDll = _SioBoxAPI_Engine_AddFakeAddon(UEngineAPI::getFunction("SioBoxAPI_Engine_AddFakeAddon"));
	if (m_addFakeAddonFromDll != nullptr) {
		try {
			m_addFakeAddonFromDll(*addonName);
			UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI_Engine_AddFakeAddon executed successfully!"));
		}
		catch (int e) {
			UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI_Engine_AddFakeAddon EXCEPTION %s"), *FString::FromInt(e))
		}
		return;
	}
	UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI_Engine_AddFakeAddon(%s) failed"), *addonName);
}

typedef void(*_SioBoxAPI_Engine_ListFakeAddons)(STRSAFE_LPSTR data[]);
static _SioBoxAPI_Engine_ListFakeAddons m_listFakeAddonsFromDll;


void UEngineAPI::listFakeAddons()
{
	UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI_Engine_ListFakeAddons"));
	m_listFakeAddonsFromDll = (_SioBoxAPI_Engine_ListFakeAddons) UEngineAPI::getFunction("SioBoxAPI_Engine_ListFakeAddons");
	if (m_listFakeAddonsFromDll != nullptr) {
		try {
			//TODO: Problem is with getting data from API. LPSTR will not be that what we want.
			STRSAFE_LPSTR ar[1];
			m_listFakeAddonsFromDll(ar);
			UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI_Engine_ListFakeAddons executed successfully! (%s)"), *ar[0]);
		}
		catch (int e) {
			UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI_Engine_ListFakeAddons EXCEPTION %s"), *FString::FromInt(e))
		}
		return;
	}
	UE_LOG(SioboxAPI, Error, TEXT("SioBoxAPI_Engine_ListFakeAddons failed"));
}

bool UEngineAPI::unloadAPI() {
	if (UEngineAPI::DllHandle != nullptr) {
		UEngineAPI::deinitializeEngine();
		m_initializeEngineFromDll = nullptr;
		FPlatformProcess::FreeDllHandle(UEngineAPI::DllHandle);
		UEngineAPI::DllHandle = nullptr;
	}

	UE_LOG(SioboxAPI, Display, TEXT("SioBoxAPI successfully unloaded."));
	return true;
}

